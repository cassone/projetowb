<?php
/**
 * Created by PhpStorm.
 * User: thiago.pereira
 * Date: 13/06/2016
 * Time: 08:46
 */


/* Solicita o par�metro "usuario"  */
if (isset($_GET['usuario']) and intval($_GET['usuario'])) {

	/* verifica se a vari�vel foi passada, ou define o valor das vari�veis */
	$numero_de_artigos = isset($_GET['artigos']) ? intval($_GET['artigos']) : 10; //10 � o padr�o
	$formato = (isset($_GET['formato']) and strtolower($_GET['formato']) == 'json') ? 'json' : 'xml'; //xml � o padr�o
	$id_do_usuario = intval($_GET['usuario']); //sem valor padr�o

	$servidor = 'localhost';
	$usuario = 'root';
	$senha = '';
	$banco = 'morpheus';
	// Conecta-se ao banco de dados MySQL
	$mysqli = new mysqli($servidor, $usuario, $senha, $banco);
	// Caso algo tenha dado errado, exibe uma mensagem de erro
	if (mysqli_connect_errno()) trigger_error(mysqli_connect_error());

	/* seleciona os artigos do banco de dados */
	$consulta = "SELECT id,nome 
							 FROM tabela 
							 WHERE id = '{$id_do_usuario}'  
							 ORDER BY id ";

	$query = $mysqli->query($consulta);
	//$resultado = mysqli_query($consulta, $mysqli) or die('Consulta com problemas:  ' . $consulta);
	/* cria um array mestre com os registros */
	$posts = array();


	if ($query->num_rows) {
		while ($post = mysqli_fetch_assoc($query)) {

			$posts[] = $post;

		}
	}


	/* extrai os dados no formato expecificado */
	if ($formato == 'json') {
		header('Content-type: application/json');
		echo json_encode($posts[0]);
	}

	/* desconecta do banco de dados */
	@mysqli_close($conexao);
}

?>